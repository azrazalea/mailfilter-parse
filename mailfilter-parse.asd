;;;; mailfilter-parse.asd

(asdf:defsystem #:mailfilter-parse
  :description "Read mailfilter files and apply the rules to mail messages."
  :author "Lily Carpenter (azrazalea)"
  :license "BSD 3-clause"
  :depends-on (#:cl-ppcre
               #:alexandria
               #:mel-base
               #:split-sequence)
  :serial t
  :components ((:file "package")
               (:file "mailfilter-parse")))
